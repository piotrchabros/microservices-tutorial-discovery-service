import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import userservice.tutorial.SpringApplicationRunner;
import userservice.tutorial.controller.UserController;
import userservice.tutorial.entity.User;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringApplicationRunner.class)
public class RestServiceTest {

    @Autowired
    UserController userController;

    @Test
    public void testRegistration()
    {
        User user = new User("stefix", "stefix", true);

        try {
            userController.register(user);
        } catch (IllegalArgumentException e)
        {
            Assert.assertEquals(e.getMessage(), "error.username");
        }
    }

}
