package userservice.tutorial.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.*;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import userservice.tutorial.service.UserService;

@Configuration
public class OAuth2ServerConfiguration {

    @Bean public JwtAccessTokenConverter tokenConverter() {
        JwtAccessTokenConverter tokenConverter = new JwtAccessTokenConverter();
        //  for asymmetric signing/verification use
        //  tokenConverter.setKeyPair(...);
        tokenConverter.setSigningKey("aTokenSigningKey");
        tokenConverter.setVerifierKey("aTokenSigningKey");
        return tokenConverter;
    }

    @Bean public TokenStore tokenStore() {
        return new JwtTokenStore(tokenConverter());
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }

    @Configuration
    @EnableResourceServer
    protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

        @Autowired UserService userDetailsService;
        @Autowired private DefaultTokenServices tokenServices;

        @Override public void configure(HttpSecurity http) throws Exception {

            http.userDetailsService(userDetailsService)
                .authorizeRequests()
                .antMatchers("/register/**")
                .permitAll()
                .antMatchers("/index/**")
                .access("#oauth2.hasScope('read')");
        }

        @Override public void configure(ResourceServerSecurityConfigurer config) {
            config.tokenServices(tokenServices);
        }

    }

    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        @Autowired private AuthenticationManager authenticationManager;
        @Autowired private JwtAccessTokenConverter tokenConverter;
        @Autowired private DefaultTokenServices tokenServices;

        @Override public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints
                .authenticationManager(authenticationManager)
                .accessTokenConverter(tokenConverter)
                .tokenServices(tokenServices);
        }

        @Override public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients
                    .inMemory()
                    .withClient("aClient")
                    .authorizedGrantTypes("password", "refresh_token")
                    .authorities("USER")
                    .scopes("read", "write")
                    //.resourceIds(RESOURCE_ID)
                    .secret("aSecret");
        }
    }
}
