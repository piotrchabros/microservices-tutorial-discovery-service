package userservice.tutorial.service;

import userservice.tutorial.entity.User;

import java.util.List;

public interface IUserService {
    List<User> getAllUsers();
    User addUser(String username, String password, boolean enabled);
    boolean usernameExists(String username);
}
