package userservice.tutorial.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import userservice.tutorial.entity.User;
import userservice.tutorial.service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/index")
    public @ResponseBody List<User> listUsers()
    {
        return userService.getAllUsers();
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/register",
            produces = APPLICATION_JSON_VALUE)
    public User register(@RequestBody User user) {

        boolean usernameExists = userService.usernameExists(user.getUsername());

        if(usernameExists)
        {
            throw new IllegalArgumentException("error.username");
        }

        return userService.addUser(user.getUsername(), user.getPassword(), user.isEnabled());
    }

    @ExceptionHandler({ IllegalArgumentException.class })
    void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

}
