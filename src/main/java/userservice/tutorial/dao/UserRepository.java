package userservice.tutorial.dao;

import javax.persistence.EntityManager;

import userservice.tutorial.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class UserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<User> getAllUsers() {
        String hql = "FROM User";
        return  (List<User>) entityManager.createQuery(hql).getResultList();
    }

    public User getUserById(int userId) {
        return entityManager.find(User.class, userId);
    }

    public User addUser(User user) {
        entityManager.persist(user);
        return user;
    }

    public void updateUser(User usr) {
        User user = getUserById(usr.getId());
        user.setPassword(usr.getPassword());
        user.setEnabled(usr.isEnabled());
        entityManager.flush();
    }

    public void deleteUser(int userId) {
        entityManager.remove(getUserById(userId));
    }

    public User getUserByUsername(String username)
    {
        String hql = "FROM User WHERE username = :username";
        return  (User) entityManager.createQuery(hql).setParameter("username", username).getResultList().get(0);
    }

    public boolean usernameExists(String username)
    {
        String hql = "FROM User WHERE username = :username";
        return  !entityManager.createQuery(hql).setParameter("username", username).getResultList().isEmpty();
    }
}
